#Yangbuilder
Ynagbuilder is groovy builder for Yang Modeling Lanuguage.

Implementation source code is in  `main/src` directory.
Test source code is in  `main/tests` directory.
Documentation files and source code is in  `documentation` directory.

Use [gradle][gradle_id] to build, test and package project.

Use [groovy][groovy_id] version 2.0.0 and later.

See `changelog.txt`.

Michal Novak (<it.novakmi@gmail.com>)

[gradle_id]: http://www.gradle.org/  "Gradle"
[groovy_id]: http://groovy.codehaus.org/  "Groovy"
