yangbuilder
===========
:Author:    Michal Novák
:Email:     it.novakmi@gmail.com
:URL:       https://bitbucket.org/novakmi/yangbuilder
:Date:      2013-03-08
:Revision:  1.0.0

This  document describes how to use Groovy builder for Yang data modeling language.
The document is written in http://www.methods.co.nz/asciidoc/[AsciiDoc].

== Version history

[options="header"]
|======
| Version     | Notes          | Date        | Author
| 0.0.1       | Initial version| 2012-04-14  | {author}
| {revision}  | Updated        | {date}      | {author}
|======

== References

[bibliography]
* [[[yang]]] http://www.netconfcentral.org/yang_docs[YANG Data Modeling Language]
* [[[RFC6020]]] RFC for http://www.ietf.org/rfc/rfc6020.txt[YANG - A Data Modeling Language for the Network Configuration Protocol (NETCONF)]
* [[[groovy]]] http://groovy.codehaus.org/[Groovy - a dynamic language for JAVA platform]
* [[[nodebuilder]]] https://bitbucket.org/novakmi/nodebuilder[Library for creation of groovy builders with plugin support]
* [[[yangbuilder]]] https://bitbucket.org/novakmi/yangbuilder[Groovy builder for YANG] - described in this document
* [[[pyang]]] http://code.google.com/p/pyang/ - Python <<yang>> parser, converter

== Terminology

* *Yang:* a Data Modeling Language for the Network Configuration Protocol (NETCONF), see <<RFC6020>>
* *Groovy builder:*  http://en.wikipedia.org/wiki/Design_Patterns[builder design pattern] implemented in <<groovy>>, with support for DSL,
  see http://groovy.codehaus.org/Builders[groovy builders]

== License

The <<yangbuilder>> is free software, licensed under http://en.wikipedia.org/wiki/MIT_License[MIT Licnense].

------------------
include::LICENSE.tmp[]
------------------

== Introduction

The <<yang>> modeling language is used to describe data model. It can be compared to the *XSD*, however, the syntax is different. 
The <<groovy>> builder can be used to create parent-child oriented data documents (e.g. *XSD*, *XML* and <<yang>>).

The syntax of the data model written in the <<yang>> resembles syntax used in the <<groovy>> builder. 

Compare <<yang>>

.Yang example from Yang tutorial (shortened)
----
container timeout {                                             <1>
    leaf access-timeout {                                       <2>
        description "Maximum time without server response";
        type uint32;
    }
    leaf retry-timer {                                          <2>
        description "Period to retry operation";
        type uint32;
    }
}
----
<1> parent element
<2> child element with sub-elements/attributes (e.g. +type+ is sub-element/attribute)

to <<groovy>> builder

.Groovy MarkupBuilder example from  http://groovy.codehaus.org/Creating+XML+using+Groovy%27s+MarkupBuilder[Creating XML] chapter
----
car(name:'HSV Maloo', make:'Holden', year:2006) {               <1>
    country('Australia')                                        <2>
    record(type:'speed', ' Truck with speed of 271kph')         <2>
}
----
<1> parent element (with attributes)
<2> child element with attributes (e.g. +type+, +value+)

NOTE: We are comparing similarity of the syntax, not the meaning. The <<yang>> example represents a data model,
      the <<groovy>> example represents a data model instance (XML with values).

TIP: Because of the syntax similarity, in some Java IDEs (http://netbeans.org/index.html[NetBeans], http://www.jetbrains.com/idea/[Intellij IDEA], http://www.eclipse.org/[Eclipse]) 
      it is possible to use groovy mode to get limited editor support for the <<yang>> files (e.g syntax highlighting).

Groovy <<yangbuilder>> allows to create <<yang>> data models in <<groovy>> language, with syntax closely similar to the <<yang>>. 
This means that one can use programming language, while creating <<yang>> data models.

.Yang example from Yang tutorial (shortened) written in <<groovy>> yangbuilder
----
container('timeout') {                                             <1>
    leaf('access-timeout') {                                       <2>
        description "Maximum time without server response";
        type uint32;
    }
    leaf('retry-timer') {                                          <2>
        description "Period to retry operation";
        type uint32;
    }
}
----
<1> parent element
<2> child element with other child sub-elements (e.g. +type+)

As you can see, there is only little syntax difference between the <<groovy>> and the <<yang>>.

NOTE: The <<groovy>> syntax in the example above was written to be as much similar to the <<yang>> as possible. 
      The difference here is only brackets around element names (only around elements containing sub-elements) and apostrophes (or quotation marks).
      In <<groovy>> you can also omit semicolon. 

<<yangbuilder>> can be used to generate <<yang>> file from <<groovy>> script (methods +getText+ or +writeToFile+). This has following pros and cons:

*Pros:*

* more advanced reuse (functions, closures, parametrization)
* use of programming language (loops, functions, variables, conditions, etc.)
* similar syntax to <<yang>>
* (groovy) support in IDE (navigation, syntax highlighting, formatting)
* plugins (write extension of the <<yangbuilder>>)

*Cons:*

* additional step - yang file has to be generated from <<groovy>> script or application
* <<yangbuilder>> does not support <<yang>> validation (one can use <<pyang>> for this)
* advanced (or better say, easily misused) reuse can lead to larger yang files (e.g. generating similar parts several times by function call instead of using yang's +grouping+)

== History

There already existed a <<groovy>> builder for the <<yang>>, which was part of one project I participated in (not publicly available). The implementation was
different with similar idea. 

When implementing <<groovy>> builder for http://plantuml.sourceforge.net/[PlantUML], called https://bitbucket.org/novakmi/plantumlbuilder[plantumlbuilder],
a common builder library <<nodebuilder>> was created. It represents base library to support implementation of textual <<groovy>> builders. The <<yangbuilder>> is based on this common library.
It is free (see LICENSE) and supports plugins (provided by <<nodebuilder>> library).

=== Dependencies

* <<groovy>> 2.0.0 and newer has to be installed on the system (preferably with +GROOVY_HOME+ set and +$GROOVY_HOME/bin+ in the the system +PATH+)
* +nodebuilder+ jar file (with +@Grab+ will be downloaded automatically)
* +yangbuilder+ jar file (with +@Grab+ will be downloaded automatically)

Note: <<nodebuilder>> and <<yangbuilder>> jar files do not need to be downloaded, if <<groovy>> +@Grab+ can be used (and an Internet connection is available)

== Installation

In most cases, there is no need for special installation of the <<yangbuilder>>, if you have installed <<groovy>> of version 2.0.0 or newer and you have an Internet connection. 
The <<groovy>> script generating <<yang>> file can download all required dependencies with use of <<groovy>> http://groovy.codehaus.org/Grape[Grape] feature. 
See +templates/scripts+ directory for example scripts. Usually, first run of the script takes some time (downloading dependencies). During next run, no dependencies are 
downloaded and an Internet connection is not needed anymore.

It is also possible to run script or <<groovy>> application in regular way by supplying +classpath+ to the dependent +jar+ files.

== Using yangbuilder

See +templates/scripts+ directory to see how to write <<yangbuilder>> scripts and generate yang files.
See +templates/project+ directory for sample project that generates several (related) yang files in one <<groovy>> application

=== Example 1

////
Syntax highlighting requires source-highlight package (Ubuntu).
java is used as it is close to groovy syntax
////
.Example groovy script to generate <<yang>> file based on example from Instant YANG tutorial
[source,java]
-----------
#!/usr/bin/env groovy                                                                              //<1>
include::acmeYang.tmp[]
-----------
<1> On Unix (Linux) you can run script to generate the <<yang>> file as any other executable script +./<scriptName>.groovy+, 
provided <<groovy>> is installed and exec attribute of the script file is set. Alternatively (or on Windows) you can run it 
as +groovy <scriptname>.groovy+.
include::acmeYang-callout.tmp[]

[source,java]
.Resulting <<yang>> file
-----------
include::acme-module.tmp[]
-----------
include::acme-module-callout.tmp[]
////
TBD footnoteref:[future]
////
=== Example 2

.Example groovy script to generate more complex <<yang>> file
[source,java]
-----------
#!/usr/bin/env groovy
include::exampleYang.tmp[]
-----------
include::exampleYang-callout.tmp[]
[source,java]
.Resulting <<yang>> file
-----------
include::example-module.tmp[]
-----------
include::example-module-callout.tmp[]

////
TBD footnoteref:[future]
////

=== <<yangbuilder>> interface methods

The <<yangbuilder>> inherits interface methods from the <<nodebuilder>>. 

Most often used <<nodebuilder>> methods are:

* +getText+ return textual representation of the <<yang>> data model
* +writeToFile+ write textual representation of the <<yang>> data model to the file
* +declareAlias+ declare alias for keyword; e.g. can be used to declare aliases for  <<yang>> keywords, which are also <<groovy>> keywords

[source,java]
---------
builder.declareAlias('import_', 'import') // declare 'import' keyword alias 'import_'
builder.'import'('my-module1') //groovy keyword has to be surrounded with quoutes
builder.import_('my-module2') //or alias can be used
---------


Following interface methods are added by the <<yangbuilder>> itself.

* +getYangName+ returns name of first +module+ or +submodule+

[source,java]
---------
builder.getYangName()
---------

* +getPrefixName+ returns name of +prefix+ of first +module+ or +submodule+ (+belongs-to+ prefix)

[source,java]
---------
builder.getPrefixName()
---------

See +YangBuilderTest.groovy+, test +yangNameTest+ test +prefixNameTest+ for examples of usage.


* +addQuoteKeywords+ specify additional keywords for <<quotes,special quotes handling>>

[source,java]
---------
builder.addQuoteKeywords('my-annotation')
---------

=== Build in attributes

In the <<yangbuilder>>, each element can have following attributes:

* +multiline+ format element string value on several lines 

[source,java]
---------
description('''The module for entities
implementing the ACME products.''', multiline: true)
---------

is expanded to:

[source,java]
---------
description
   "The module for entities
    implementing the ACME products.";
---------

* +indent+ enable/disable indentation for the element (indentation is enabled by default, unless +multiline+ attribute is used)

[source,java]
---------
description(
    '''test quotes
in multiline
description''', multiline: true, indent: true)
---------
* +quotes+ force to surround element value with quotes (with provided quotes character), even though quotes are not needed;
  see also <<quotes,special quotes handling>>.

[source,java]
---------
organization 'novakmi'
contact('it.novakmi@gmail.com', quotes: '"')
---------

* +noAutoQuotes+ do not add automatic quotes for element with <<quotes,special quotes handling>>

[source,java]
---------
description('test quotes', noAutoQuotes: true)
---------
WARNING: Element statements without quotes may lead to an invalid yang.

* +cmt+ add inline comment to the element, the comment is on the same line, see also <<cmt,cmt keyword>>

[source,java]
---------
container('socket', cmt: "Inline comment for socket container")
---------

See +YangBuilderTest.groovy+, tests +quoteTest+ and +commentTest+ for other examples and corresponding <<yang>> result.

=== Keywords

Some keywords have special meaning, when used with <<yangbuilder>>

==== +yngbuild+ keyword

This keyword echoes its value directly to the yang file. This is useful, if it is not possible to create content of the  <<yang>> file  
with regular builder syntax. The +yngbuild+ keyword accepts optional attribute +indent+. If +indent+ value evaluates to +true+, the elements 
value is indented according to the current nesting level, otherwise no indentation is performed.

Example:

+builder.yngbuild("/* not indented comment */")+

+builder.yngbuild("/* indented comment */", indent:true)+

NOTE: It is preferred to use +<<cmt,cmt keyword>>+ keyword for comments, rather than +yngbuild+ keyword

[[quotes]]
==== <<yang>> keywords with special handling for quotes
 
The value of following <<yang>> keywords will be automatically surrounded with quotes, if needed. This is done when string contains any space or tab characters, a semicolon (";"), 
braces ("{" or "}") or comment sequences ("//", "/*", or "*/"). 

Double quotes are preferred, if string already contains double quotes, single quotes are used. 

See <<RFC6020>> section.6.1.3.

* +namespace+
* +key+
* +pattern+
* +prefix+
* +reference+
* +contact+
* +description+
* +presence+
* +organization+

Example: 

+description "description of the model"+

[[cmt]]
==== +cmt+ keyword

+cmt+ keyword is similar to the +yngbuild+ keyword. It is intended to simplify writing of the <<yang>> comments.

As default, one-line indented comment is produced:

+cmt("This is inline indented comment")+ (default)

Other options are:

+cmt("This is inline, not indented comment", indent: false)+

+cmt("This is non-inline indented comment", inline: false)+

+cmt("This is non-inline comment", not indent: false, inline: false)+

Inline comments have +//+ as comment mark, non-inline comments use +/\*+ and +*/+ on separate lines.
Indented comments are indented according to the current indent (level of nesting).

See +YangBuilderTest.groovy+, test +commentTest+ for examples and corresponding <<yang>> result.


== Plugins

Plugins can extend <<yangbuilder>> with additional functionality.  Plugin has to implement +NodeBuilderPlugin+ abstract 
class interface (part of <<nodebuilder>>). 

Plugins are registered with +registerPlugin+ method or they can be 
passed as second argument, when creating the builder (it is possible to pass one instance or list of plugin instances). 

Example:

[source,java]
---------
def builder = new YangBuilder(2, new CompactYangPlugin()) 
---------

Plugins, which are part of the <<yangbuilder>> distribution, are described in the next subsections.

=== CompactYangPlugin

This plugin allows to shorten <<yang>> syntax and write yang in more compact way.
In most cases, it allows to write sub-elements as attributes.

Example:

[source,java]
---------
leaf('node', type: string)
---------

which is equivalent to

[source,java]
---------
leaf('node') {
  type string;
}
---------

.Example groovy script to generate more complex <<Yang>> file with +CompactYangPlugin+
[source,java]
-----------
include::exampleCompactYang.tmp[]
-----------
include::exampleCompactYang-callout.tmp[]

////
 syntax table
////


See +CompactYangPlugin.groovy+ for implementation and 
+CompactYangPluginTest.groovy+ for more plugin usage and examples.


=== GroupingResolverPlugin

This pugins resolves all +groupings+ within the builder and returns new +BuilderNode+ which represents root of
the <<yang>> model, in which all +groupings+ are expanded (resolved).

See +GroupingResolverPlugin+ for example of plugin implementation and
+GroupingResolverPluginTest.groovy+ for plugin usage examples.

